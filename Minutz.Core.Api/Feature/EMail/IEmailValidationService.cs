namespace Minutz.Core.Api.Feature.EMail
{
    public interface IEmailValidationService
    {
        bool Valid(string email);
    }
}