using Minutz.Core.Api.Models.Entities;

namespace Minutz.Core.Api.Feature.MeetingAttendee
{
    public interface IMinutzAvailabilityService
    {
        AttendeeMessage GetAvailableAttendees(AuthRestModel user, string schema);
    }
}