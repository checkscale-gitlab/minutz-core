namespace Minutz.Core.Api.Feature.MeetingAttendee
{
    public interface IMinutzAvailabilityRepository
    {
        AttendeeMessage GetAvailableAttendees(string schema, string connectionString, string masterConnectionString);

        AttendeeMessage CreateAvailableAttendee(Models.Entities.MeetingAttendee attendee, string schema, string connectionString);
    }
}