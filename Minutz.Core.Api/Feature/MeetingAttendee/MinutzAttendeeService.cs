using System;
using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Feature.MeetingAttendee
{
    public class MinutzAttendeeService : IMinutzAttendeeService
    {
          private readonly IConnectionStringService _applicationSetting;
        private readonly IMinutzAttendeeRepository _minutzAttendeeRepository;

        public MinutzAttendeeService(IConnectionStringService applicationSetting,
                                     IMinutzAttendeeRepository minutzAttendeeRepository)
        {
            _applicationSetting = applicationSetting;
            _minutzAttendeeRepository = minutzAttendeeRepository;
        }

        public AttendeeMessage GetAttendees(Guid meetingId, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString ( schema, _applicationSetting.GetInstancePassword (schema));
            var masterConnectionString = _applicationSetting.CreateConnectionString();
            return _minutzAttendeeRepository.GetAttendees(meetingId, schema, instanceConnectionString, masterConnectionString);
        }

        public AttendeeMessage AddAttendee(Guid meetingId, Models.Entities.MeetingAttendee attendee ,AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString (schema, _applicationSetting.GetInstancePassword (schema));
            return _minutzAttendeeRepository.AddAttendee(meetingId, attendee, schema, instanceConnectionString);
        }
        
        public AttendeeMessage UpdateAttendee(Guid meetingId, Models.Entities.MeetingAttendee attendee ,AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString (schema, _applicationSetting.GetInstancePassword (schema));
            var masterConnectionString = _applicationSetting.CreateConnectionString();
            return _minutzAttendeeRepository.UpdateAttendee(meetingId, attendee, schema, instanceConnectionString, masterConnectionString);
        }

        public MessageBase DeleteAttendee(Guid meetingId, string attendeeEmail, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString ( schema, _applicationSetting.GetInstancePassword (schema));
            return _minutzAttendeeRepository.DeleteAttendee(meetingId, attendeeEmail, schema, instanceConnectionString);
        }
    }
}