using System;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Feature.MeetingAttendee
{
    public interface IMinutzAttendeeRepository
    {
        AttendeeMessage GetAttendees(Guid meetingId, string schema, string connectionString, string masterConnectionString);
        
        AttendeeMessage AddAttendee(Guid meetingId, Models.Entities.MeetingAttendee attendee, string schema, string connectionString);

        AttendeeMessage UpdateAttendee(Guid meetingId, Models.Entities.MeetingAttendee attendee, string schema,
            string connectionString, string masterConnectionString);
        
        MessageBase DeleteAttendee(Guid meetingId, string attendeeEmail, string schema, string connectionString);
    }
}