using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Models.Entities;

namespace Minutz.Core.Api.Feature.MeetingAttendee
{
    public class MinutzAvailabilityService: IMinutzAvailabilityService
    {
        private readonly IConnectionStringService _applicationSetting;
        private readonly IMinutzAvailabilityRepository _minutzAvailabilityRepository;

        public MinutzAvailabilityService(IConnectionStringService applicationSetting,
            IMinutzAvailabilityRepository minutzAvailabilityRepository)
        {
            _applicationSetting = applicationSetting;
            _minutzAvailabilityRepository = minutzAvailabilityRepository;
        }

        /// <summary>
        /// Get the attendees that can be used for a meeting
        /// </summary>
        /// <param name="user">Current logged in user</param>
        /// <returns>Collection of people that can be used for a meeting</returns>
        public AttendeeMessage GetAvailableAttendees( AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString (schema, _applicationSetting.GetInstancePassword (schema));
            var masterConnectionString = _applicationSetting.CreateConnectionString();
            return _minutzAvailabilityRepository.GetAvailableAttendees(schema, instanceConnectionString, masterConnectionString);
        }
    }
}