using System.Collections.Generic;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Feature.Action
{
    public class ActionMessage : MessageBase
    {
        public MinutzAction Action { get; set; }
        public IEnumerable<MinutzAction> Actions { get; set; }
    }
}