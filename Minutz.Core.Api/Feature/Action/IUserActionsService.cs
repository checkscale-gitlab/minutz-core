using Minutz.Core.Api.Models.Entities;

namespace Minutz.Core.Api.Feature.Action
{
    public interface IUserActionsService
    {
        ActionMessage Actions(AuthRestModel user);
    }
}