using System;
using System.ComponentModel.DataAnnotations;

namespace Minutz.Core.Api.Feature.Action
{
    public class UpdateActionRequest
    {
        [Required]
        public Guid Id { get; set; }
        
        public dynamic Value { get; set; }
        
        public string InstanceId { get; set; }
    }
}