using System;
using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Contracts.Note;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Feature.Meeting.Note
{
    public class MinutzNoteService : IMinutzNoteService
    {
        private readonly IMinutzNoteRepository _noteRepository;
        private readonly IConnectionStringService _applicationSetting;

        public MinutzNoteService(IConnectionStringService applicationSetting,
                                     IMinutzNoteRepository noteRepository)
        {
            _noteRepository = noteRepository;
            _applicationSetting = applicationSetting;
        }
        
        public NoteMessage GetMeetingNotes(Guid meetingId, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString( schema, _applicationSetting.GetInstancePassword(schema));
            return _noteRepository.GetNoteCollection(meetingId, user.InstanceId, instanceConnectionString);
        }

        public NoteMessage QuickNoteCreate(Guid meetingId, string noteText, int order, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString( schema, _applicationSetting.GetInstancePassword(schema));
            return _noteRepository.QuickCreateNote(meetingId, noteText, order, user.InstanceId, instanceConnectionString);
        }

        public NoteMessage UpdateNote(Guid meetingId, MeetingNote note, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString( schema, _applicationSetting.GetInstancePassword(schema));
            return _noteRepository.UpdateNote(meetingId, note, user.InstanceId, instanceConnectionString);
        }
        
        public MessageBase DeleteNote(Guid noteId, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString( schema, _applicationSetting.GetInstancePassword(schema));
            return _noteRepository.DeleteNote(noteId, user.InstanceId, instanceConnectionString);
        }
    }
}