using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Contracts.Tag;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Feature.Meeting.Tag
{
    public class MinutzTagService :IMinutzTagService
    {
        private readonly IMinutzTagRepository _minutzTagRepository;
        private readonly IConnectionStringService _applicationSetting;

        public MinutzTagService(IMinutzTagRepository minutzTagRepository, IConnectionStringService applicationSetting)
        {
            _minutzTagRepository = minutzTagRepository;
            _applicationSetting = applicationSetting;
        }

        public MessageBase Update(string meetingId, string tags, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString(schema, _applicationSetting.GetInstancePassword(schema));
            return _minutzTagRepository.Update(meetingId, tags, user.InstanceId, instanceConnectionString);
        }
    }
}