using System;

namespace Minutz.Core.Api.Feature.Meeting
{
    public interface IUserMeetingsRepository
    {
        MeetingMessage Meetings(string email, string schema, string connectionString);

        MeetingMessage Meeting(Guid meetingId, string schema, string connectionString);

        MeetingMessage AttendeeMeetings(string email, string schema, string connectionString);
    
        MeetingMessage CreateEmptyUserMeeting(string email, string schema, string connectionString);
    }
}