using System;
using Minutz.Core.Api.Contracts;

namespace Minutz.Core.Api.Feature.Meeting
{
    public class GetMeetingService: IGetMeetingService
    {
        
        private readonly IConnectionStringService _applicationSetting;
        private readonly IGetMeetingRepository _getMeetingRepository;

        public GetMeetingService(IConnectionStringService applicationSetting, IGetMeetingRepository getMeetingRepository)
        {
            _applicationSetting = applicationSetting;
            _getMeetingRepository = getMeetingRepository;
        }

        public MeetingMessage GetMeeting(string instanceId, Guid meetingId)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString(instanceId, _applicationSetting.GetInstancePassword(instanceId));
            return _getMeetingRepository.Get(meetingId, instanceId, instanceConnectionString);
        }
    }
}