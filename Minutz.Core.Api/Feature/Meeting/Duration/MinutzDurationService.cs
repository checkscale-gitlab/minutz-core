using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Contracts.Duration;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Feature.Meeting.Duration
{
    public class MinutzDurationService: IMinutzDurationService
    {
        private readonly IMinutzDurationRepository _minutzDurationRepository;
        private readonly IConnectionStringService _applicationSetting;

        public MinutzDurationService(IMinutzDurationRepository minutzTimeRepository, IConnectionStringService applicationSetting)
        {
            _minutzDurationRepository = minutzTimeRepository;
            _applicationSetting = applicationSetting;
        }

        public MessageBase Update(string meetingId, int duration, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString( schema, _applicationSetting.GetInstancePassword(schema));
            return _minutzDurationRepository.Update(meetingId, duration, user.InstanceId, instanceConnectionString);
        }
    }
}