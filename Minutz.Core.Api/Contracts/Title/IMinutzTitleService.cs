using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Contracts.Title
{
    public interface IMinutzTitleService
    {
        MessageBase Update(string meetingId, string title, AuthRestModel user, string schema);
    }
}