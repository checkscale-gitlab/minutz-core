using System;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Contracts.Date
{
    public interface IMinutzDateService
    {
        MessageBase Update(string meetingId, DateTime date, AuthRestModel user, string schema);
    }
}