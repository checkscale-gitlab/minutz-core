using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Contracts.Recurrence
{
    public interface IMeetingRecurrenceRepository
    {
        MessageBase Update(string meetingId, int recurrenceType, string schema, string connectionString);
    }
}