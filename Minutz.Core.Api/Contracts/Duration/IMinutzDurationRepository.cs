using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Contracts.Duration
{
    public interface IMinutzDurationRepository
    {
        MessageBase Update(string meetingId, int duration, string schema, string connectionString);
    }
}