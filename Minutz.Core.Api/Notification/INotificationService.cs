using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Notification
{
    public interface INotificationService
    {
        MessageBase SendInstanceInvitation(MeetingAttendee instancePerson, string instanceId, string company);
    }
}