﻿using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Contracts.Date;
using Minutz.Core.Api.Contracts.Decision;
using Minutz.Core.Api.Contracts.Duration;
using Minutz.Core.Api.Contracts.Location;
using Minutz.Core.Api.Contracts.Note;
using Minutz.Core.Api.Contracts.ObjectivePurpose;
using Minutz.Core.Api.Contracts.Recurrence;
using Minutz.Core.Api.Contracts.Tag;
using Minutz.Core.Api.Contracts.Time;
using Minutz.Core.Api.Contracts.Title;
using Minutz.Core.Api.Controllers;
using Minutz.Core.Api.Feature.Action;
using Minutz.Core.Api.Feature.Agenda;
using Minutz.Core.Api.Feature.EMail;
using Minutz.Core.Api.Feature.Invitation;
using Minutz.Core.Api.Feature.Meeting;
using Minutz.Core.Api.Feature.Meeting.Date;
using Minutz.Core.Api.Feature.Meeting.Decision;
using Minutz.Core.Api.Feature.Meeting.Duration;
using Minutz.Core.Api.Feature.Meeting.Location;
using Minutz.Core.Api.Feature.Meeting.Note;
using Minutz.Core.Api.Feature.Meeting.ObjectivePurpose;
using Minutz.Core.Api.Feature.Meeting.Recurrence;
using Minutz.Core.Api.Feature.Meeting.Tag;
using Minutz.Core.Api.Feature.Meeting.Time;
using Minutz.Core.Api.Feature.Meeting.Title;
using Minutz.Core.Api.Feature.MeetingAttendee;
using Minutz.Core.Api.Http;
using Minutz.Core.Api.Notification;
using Minutz.Core.Api.Security;
using Minutz.Core.Api.User;
using Minutz.Core.Feature.Agenda;
using Minutz.Core.Feature.Http;
using Minutz.Core.Feature.Meeting;
using Minutz.Core.Feature.Person;
using Minutz.Core.Feature.Related;
using Minutz.Core.Feature.SignUp;
using Minutz.Core.Feature.UserAdmin;
using Serilog;
using Serilog.Sinks.Elasticsearch;
using Swashbuckle.AspNetCore.Swagger;

namespace Minutz.Core.Api {
    public class Startup {
        public Startup (IConfiguration configuration) {
            this.Configuration = configuration;
            //this.Version = version;

        }
        public IConfiguration Configuration { get; }
        private const string Title = "Minutz Core Api";
        private string Version { get; set; }

        public Startup (IHostingEnvironment env) {
            var builder = new ConfigurationBuilder ()
                .SetBasePath (env.ContentRootPath)
                .AddJsonFile ("appsettings.json", optional : false, reloadOnChange : true)
                .AddJsonFile ($"appsettings.{env.EnvironmentName}.json", optional : true)
                .AddEnvironmentVariables ();

            Configuration = builder.Build ();
            var elasticUri = Configuration["ElasticConfiguration:Uri"];
            Log.Logger = new LoggerConfiguration ()
                .Enrich.FromLogContext ()
                .WriteTo.Elasticsearch (new ElasticsearchSinkOptions (new Uri ("https://elasticsearch.minutz.net/")) {
                    AutoRegisterTemplate = true,
                })
                .WriteTo.Console (
                    outputTemplate: "{Timestamp:HH:mm} [{Level}] ({ThreadId}) {Message}{NewLine}{Exception}")
                .CreateLogger ();
            Version = Configuration.GetSection ("Version").Value;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices (IServiceCollection services) {
            var appSetting = new ConnectionStringService (new InstanceRepository ());

            services.ConfigurePersonInstanceValidationServices ();
            services.ConfigureAdminHttpServiceServices ();

            services.AddTransient<IEmailValidationService, EmailValidationService> ();
            services.AddTransient<ICipherService, CipherService> ();
            services.AddTransient<ICustomPasswordValidator, CustomPasswordValidator> ();
            services.AddTransient<IHttpService, HttpService> ();

            services.AddTransient<IUserRepository, UserRepository> ();
            services.AddTransient<IInstanceRepository, InstanceRepository> ();

            //Services
            services.AddTransient<IConnectionStringService, ConnectionStringService> ();
            services.AddTransient<INotify, Notify> ();
            services.AddTransient<INotificationService, NotificationService> ();

            //*
            // Features
            // ---- Meeting
            //*
            //Meeting Title
            services.AddTransient<IMinutzTitleRepository, MinutzTitleRepository> ();
            services.AddTransient<IMinutzTitleService, MinutzTitleService> ();
            //Meeting Location
            services.AddTransient<IMinutzLocationRepository, MinutzLocationRepository> ();
            services.AddTransient<IMinutzLocationService, MinutzLocationService> ();
            //Meeting Date
            services.AddTransient<IMinutzDateRepository, MinutzDateRepository> ();
            services.AddTransient<IMinutzDateService, MinutzDateService> ();
            //Meeting Time
            services.AddTransient<IMinutzTimeRepository, MinutzTimeRepository> ();
            services.AddTransient<IMinutzTimeService, MinutzTimeService> ();
            //Meeting Time
            services.AddTransient<IMinutzDurationRepository, MinutzDurationRepository> ();
            services.AddTransient<IMinutzDurationService, MinutzDurationService> ();
            //Meeting Tag
            services.AddTransient<IMinutzTagRepository, MinutzTagRepository> ();
            services.AddTransient<IMinutzTagService, MinutzTagService> ();

            //*
            // Features
            // ---- Dashboard
            //*
            //User Meetings
            services.AddTransient<IUserMeetingsRepository, UserMeetingsRepository> ();
            services.AddTransient<IUserMeetingsService, UserMeetingsService> ();
            //User Actions
            services.AddTransient<IUserActionsRepository, UserActionsRepository> ();
            services.AddTransient<IUserActionsService, UserActionsService> ();

            //*
            // Features
            // ---- Meeting
            // ------- Agenda
            //*MinutzAgenda
            services.AddTransient<IMinutzAgendaRepository, MinutzAgendaRepository> ();
            services.AddTransient<IMinutzAgendaService, MinutzAgendaService> ();
            //*MinutzAction
            services.AddTransient<IMinutzActionRepository, MinutzActionRepository> ();
            services.AddTransient<IMinutzActionService, MinutzActionService> ();
            //User Meeting Manager
            services.AddTransient<IUserManageMeetingRepository, UserManageMeetingRepository> ();
            services.AddTransient<IUserManageMeetingService, UserManageMeetingService> ();
            // Objective Purpose
            services.AddTransient<IMeetingObjectivePurposeRepository, MeetingObjectivePurposeRepository> ();
            services.AddTransient<IMeetingObjectivePurposeService, MeetingObjectivePurposeService> ();

            services.AddTransient<IMinutzAvailabilityRepository, MinutzAvailabilityRepository> ();
            services.AddTransient<IMinutzAvailabilityService, MinutzAvailabilityService> ();

            services.AddTransient<IMinutzAttendeeRepository, MinutzAttendeeRepository> ();
            services.AddTransient<IMinutzAttendeeService, MinutzAttendeeService> ();

            services.AddTransient<IMeetingRecurrenceRepository, MeetingRecurrenceRepository> ();
            services.AddTransient<IMinutzRecurrenceService, MinutzRecurrenceService> ();

            services.AddTransient<IGetMeetingRepository, GetMeetingRepository> ();
            services.AddTransient<IGetMeetingService, GetMeetingService> ();

            services.AddTransient<IMinutzDecisionRepository, MinutzDecisionRepository> ();
            services.AddTransient<IMinutzDecisionService, MinutzDecisionService> ();

            services.AddTransient<IMinutzNoteRepository, MinutzNoteRepository> ();
            services.AddTransient<IMinutzNoteService, MinutzNoteService> ();

            services.AddTransient<IInvitationService, InvitationService> ();

            services.AddTransient<IInstanceUserRepository, InstanceUserRepository> ();
            services.AddTransient<IInstanceUserService, InstanceUserService> ();

            services.ConfigureSignUpServiceServices ();
            services.ConfigureUserAdminServices ();
            services.ConfigurePersonServices ();

            services.AddSwaggerGen (c => { c.SwaggerDoc ("v1", new Info { Title = Title, Version = Version }); });
            services.AddCors (options => {
                options.AddPolicy ("AllowAllOrigins",
                    builder => {
                        builder
                            .AllowAnyMethod ()
                            .AllowAnyHeader ()
                            .WithOrigins ("http://localhost:4200", "https://beta.minutz.net")
                            .AllowCredentials ();
                    });
            });

            services.AddDbContext<ApplicationDbContext> ();
            services.AddIdentity<IdentityUser, IdentityRole> ()
                .AddEntityFrameworkStores<ApplicationDbContext> ()
                .AddDefaultTokenProviders ();
            services
                .AddAuthentication (options => {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

                })
                .AddJwtBearer (options => {
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;
                    options.TokenValidationParameters = new TokenValidationParameters {
                        ValidIssuer =
                        appSetting.AuthorityDomain,
                        ValidAudience =
                        appSetting.AuthorityDomain,
                        IssuerSigningKey =
                        new SymmetricSecurityKey (
                        Encoding.UTF8.GetBytes (appSetting
                        .ClientSecret)),
                        ClockSkew = TimeSpan.Zero
                    };
                    options.Events = new JwtBearerEvents {
                        OnMessageReceived = context => {
                            var accessToken = context.Request.Query["access_token"];

                            var path = context.HttpContext.Request.Path;
                            if (!string.IsNullOrEmpty (accessToken) &&
                                (path.StartsWithSegments ("/messageHub"))) {
                                // Read the token out of the query string AdminHub
                                context.Token = accessToken;
                            }
                            if (!string.IsNullOrEmpty (accessToken) &&
                                (path.StartsWithSegments ("/adminHub"))) {
                                // Read the token out of the query string 
                                context.Token = accessToken;

                            }
                            return Task.CompletedTask;
                        }
                    };
                });
            services.AddTransient<AuthorizationHeaderFilter> ();
            services.AddMvc (options => {
                options.Filters.AddService<AuthorizationHeaderFilter> ();
            }).SetCompatibilityVersion (CompatibilityVersion.Version_2_2);
            services.AddSignalR (hubOptions => {
                hubOptions.EnableDetailedErrors = true;
                hubOptions.KeepAliveInterval = TimeSpan.FromSeconds (1);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure (IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, ApplicationDbContext dbContext) {
            loggerFactory.AddSerilog ();
            app.UseSignalRQueryStringAuth ();
            if (env.IsDevelopment ()) {
                app.UseDeveloperExceptionPage ();
            }
            app.UseSwagger ();
            app.UseSwaggerUI (c => { c.SwaggerEndpoint ("/swagger/v1/swagger.json", Version); });
            app.UseCors ("AllowAllOrigins");
            app.UseAuthentication ();

            app.UseSignalR ((configure) => {
                var desiredTransports =
                    HttpTransportType.WebSockets |
                    HttpTransportType.LongPolling;

                configure.MapHub<MessageHub> ("/messageHub", (options) => {
                    options.Transports = desiredTransports;
                });
                configure.MapHub<AdminHub> ("/adminHub", (options) => {
                    options.Transports = desiredTransports;
                });
            });
            app.UseMvc (routes => {
                routes.MapRoute (
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            dbContext.Database.EnsureCreated ();
        }
    }
}