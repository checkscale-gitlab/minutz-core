using System.Collections.Generic;
using Minutz.Core.Api.Models.Entities;

namespace Minutz.Core.Api.Models.Message
{
    public class InstanceResponse : MessageBase
    {
        public Instance Instance { get; set; }
        public List<Instance> Instances { get; set; }
    }
}