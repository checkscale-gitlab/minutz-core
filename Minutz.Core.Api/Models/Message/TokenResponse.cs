using Minutz.Core.Api.Models.Entities;

namespace Minutz.Core.Api.Models.Message
{
    public class TokenResponse : MessageBase
    {
        public  UserResponseModel AuthTokenResponse { get; set; }
    }
}