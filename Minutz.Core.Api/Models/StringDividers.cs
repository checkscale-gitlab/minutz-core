namespace Minutz.Core.Api.Models
{
    public static class StringDividers
    {
        public const string InstanceStringDivider = "&";
        public const string MeetingStringDivider = ";";
    }
}