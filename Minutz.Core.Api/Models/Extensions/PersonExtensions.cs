using System.Collections.Generic;
using Minutz.Core.Feature.Person;

namespace Minutz.Core.Api.Models.Extensions
{
    public static class PersonExtensions
    {
        public static List<(string instanceId, string meetingId)> RelatedItems(this Person person)
        {
            return person.Related.SplitToList (Entities.StringDividers.InstanceStringDivider, Entities.StringDividers.MeetingStringDivider);
        }
    }
}