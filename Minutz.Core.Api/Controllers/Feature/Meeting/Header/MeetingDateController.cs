﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Contracts.Date;
using Minutz.Core.Api.Models.ViewModel;

namespace Minutz.Core.Api.Controllers.Feature.Meeting.Header
{
    
    public class MeetingDateController : BaseController
    {
        private readonly IMinutzDateService _minutzDateService;

        public MeetingDateController(IMinutzDateService minutzDateService)
        {
            _minutzDateService = minutzDateService;
        }

        [Authorize]
        [HttpPost("api/feature/header/date", Name = "Update Meeting date")]
        // ReSharper disable once MVC1004
        public IActionResult UpdateMeetingDateResult([FromBody]DateViewModel model)
        {
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(model.instanceId)) schema = AuthUser.InfoResponse.InstanceId;
            var result = _minutzDateService.Update(model.id, model.meetingDate, AuthUser.InfoResponse, schema);
            if (result.Condition)
            {
                return Ok();
            }

            return StatusCode(result.Code, result.Message);
        }
    }
}