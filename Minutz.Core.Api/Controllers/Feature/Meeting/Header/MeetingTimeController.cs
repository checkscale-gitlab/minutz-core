﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Contracts.Time;

namespace Minutz.Core.Api.Controllers.Feature.Meeting.Header
{
    
    public class MeetingTimeController : BaseController
    {
        private readonly IMinutzTimeService _minutzTimeService;
        
        public MeetingTimeController(IMinutzTimeService minutzTimeService)
        {
            _minutzTimeService = minutzTimeService;
        }

        // [Authorize]
        // [HttpPost("api/feature/header/time", Name = "Update Meeting time")]
        // public IActionResult UpdateMeetingTimeResult(string id, string time, string instanceId)
        // {
        //     var schema = AuthUser.InfoResponse.InstanceId;
        //     if (!string.IsNullOrEmpty(instanceId)) schema = AuthUser.InfoResponse.InstanceId;
        //     var result = _minutzTimeService.Update(id, time, AuthUser.InfoResponse, schema);
        //     if (result.Condition)
        //     {
        //         return Ok();
        //     }

        //     return StatusCode(result.Code, result.Message);
        // }
    }
}