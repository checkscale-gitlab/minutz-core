﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Contracts.Duration;

namespace Minutz.Core.Api.Controllers.Feature.Meeting.Header
{
    
    public class MeetingDurationController : BaseController
    {
        private readonly IMinutzDurationService _minutzDurationService;
        
        public MeetingDurationController(IMinutzDurationService minutzDurationService)
        {
            _minutzDurationService = minutzDurationService;
        }

        // [Authorize]
        // [HttpPost("api/feature/header/duration", Name = "Update Meeting duration")]
        // public IActionResult UpdateMeetingDurationResult(string id, int duration, string instanceId)
        // {
        //     var schema = AuthUser.InfoResponse.InstanceId;
        //     if (!string.IsNullOrEmpty(instanceId)) schema = AuthUser.InfoResponse.InstanceId;
        //     var result = _minutzDurationService.Update(id, duration, AuthUser.InfoResponse, schema);
        //     if (result.Condition)
        //     {
        //         return Ok();
        //     }

        //     return StatusCode(result.Code, result.Message);
        // }
    }
}