﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Contracts.Tag;

namespace Minutz.Core.Api.Controllers.Feature.Meeting.Header
{
    
    public class MeetingTagController : BaseController
    {
        private readonly IMinutzTagService _minutzTagService;
        
        public MeetingTagController(IMinutzTagService minutzTagService)
        {
            _minutzTagService = minutzTagService;
        }

        [Authorize]
        [HttpPost("api/feature/header/tag", Name = "Update Meeting tag")]
        public IActionResult UpdateMeetingTagResult(string id, string tag, string instanceId)
        {
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(instanceId)) schema = AuthUser.InfoResponse.InstanceId;
            var result = _minutzTagService.Update(id, tag, AuthUser.InfoResponse, schema);
            if (result.Condition)
            {
                return Ok();
            }

            return StatusCode(result.Code, result.Message);
        }
    }
}