using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Contracts.Status;

namespace Minutz.Core.Api.Controllers.Feature.Meeting.Status
{
    
    public class MeetingStatusController : BaseController
    {
        private readonly IMeetingStatusService _meetingStatusService;

        public MeetingStatusController(IMeetingStatusService meetingStatusService)
        {
            _meetingStatusService = meetingStatusService;
        }

        [Authorize]
        [HttpPost("api/feature/status/update", Name = "Update meeting Status Update")]
        public IActionResult UpdateMeetingStatusResult(string id, string status, string instanceId)
        {
            if (string.IsNullOrEmpty(id))
                return StatusCode(401, "Request is missing values for the request");
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(instanceId)) schema = instanceId;
            var result = _meetingStatusService.UpdateMeetingStatus
                (Guid.Parse(id), status, AuthUser.InfoResponse, schema);
            return result.Condition ? (IActionResult) Ok() : StatusCode(result.Code, result.Message);
        }
    }
}
