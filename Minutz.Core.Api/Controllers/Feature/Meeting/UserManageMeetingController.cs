using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Minutz.Core.Api.Feature.Meeting;
using Minutz.Core.Api.Models.Entities;

namespace Minutz.Core.Api.Controllers.Feature.Meeting
{
  
  public class UserManageMeetingController : BaseController
  {
    private readonly IUserMeetingsService _userMeetingsService;
    private readonly IUserManageMeetingService _userManageMeetingService;

    public UserManageMeetingController(
      IUserMeetingsService userMeetingsService,
      IUserManageMeetingService userManageMeetingService)
    {
      _userMeetingsService = userMeetingsService;
      _userManageMeetingService = userManageMeetingService;
    }
  
    [Authorize]
    [HttpGet("api/feature/managemeeting/user", Name = "User Meeting Item")]
    public IActionResult UserMeetingResult(Guid meetingId, string instanceId)
    {
      var schema = AuthUser.InfoResponse.InstanceId;
      if (!string.IsNullOrEmpty(instanceId)) schema = instanceId;
      var result = _userMeetingsService.Meeting(AuthUser.InfoResponse, meetingId, schema);
      if (result.Condition)
      {
        return Ok(result.Meeting);
      }

      return StatusCode(result.Code, result.Message);
    }

    [Authorize]
    [HttpPost("api/feature/managemeeting/user", Name = "User Meeting Item")]
    public IActionResult UserMeetingResult([FromBody] MeetingUpdateRequest meeting)
    {
      var schema = AuthUser.InfoResponse.InstanceId;
      if (!string.IsNullOrEmpty(meeting.instanceId)) schema = meeting.instanceId;
      
      var meetingEntity = new Api.Feature.Meeting.Meeting
                          {
                            Id = meeting.id,
                            InstanceId = schema,
                            IsFormal = meeting.isFormal,
                            IsLocked = meeting.isLocked,
                            IsPrivate = meeting.isPrivate,
                            IsRecurrence = meeting.isRecurrence,
                            Tag = meeting.tag == null? "" : string.Join(";",meeting.tag),
                            Duration = meeting.duration,
                            Date = meeting.date,
                            Location = string.IsNullOrEmpty(meeting.location)? string.Empty: meeting.location,
                            Name = string.IsNullOrEmpty(meeting.name)? "Minutz Meeting": meeting.name,
                            Outcome = string.IsNullOrEmpty(meeting.outcome)? string.Empty: meeting.outcome,
                            Purpose = string.IsNullOrEmpty(meeting.purpose)? string.Empty: meeting.purpose,
                            Status = string.IsNullOrEmpty(meeting.status)? "Create": meeting.status,
                            Time = string.IsNullOrEmpty(meeting.time)? DateTime.UtcNow.ToString(): meeting.time,
                            UpdatedDate = DateTime.UtcNow,
                            RecurrenceType = meeting.recurrenceType.ToString(),
                            MeetingOwnerId = string.IsNullOrEmpty(meeting.meetingOwnerId)? AuthUser.InfoResponse.Email: meeting.meetingOwnerId,
                            TimeZone = string.IsNullOrEmpty(meeting.timeZone)? string.Empty: meeting.timeZone
                          };


      var result = _userManageMeetingService.UpdateMeeting(meetingEntity, AuthUser.InfoResponse, schema);
      if (result.Condition)
      {
        return Ok(result.Meeting);
      }

      return StatusCode(result.Code, result.Message);
    }
  }
}