using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Minutz.Core.Api.Feature.MeetingAttendee;

namespace Minutz.Core.Api.Controllers.Feature.Meeting
{
    
    public class MinutzAvailabilityController : BaseController
    {
        private readonly IMinutzAvailabilityService _minutzAvailabilityService;

        public MinutzAvailabilityController(IMinutzAvailabilityService minutzAvailabilityService)
        {
            _minutzAvailabilityService = minutzAvailabilityService;
        }

        /// <summary>
        /// Get the available attendees for a meeting
        /// </summary>
        /// <returns>Collection of MeetingAttendees</returns>
        [Authorize]
        [HttpGet("api/feature/availability/attendees", Name = "Get Available Attendees")]
        public IActionResult GetAvailableAttendeesResult(string instanceId)
        {
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(instanceId)) schema = AuthUser.InfoResponse.InstanceId;
            var result = _minutzAvailabilityService.GetAvailableAttendees(AuthUser.InfoResponse, schema);
            return result.Condition ? Ok(result.Attendees) : StatusCode(result.Code, result.Message);
        }
    }
}