﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Minutz.Core.Api.Feature.Meeting;

namespace Minutz.Core.Api.Controllers.Feature.Dashboard.Meeting
{
    public class UserMeetingController : BaseController
    {
        private readonly IUserMeetingsService _userMeetingsService;

        public UserMeetingController(IUserMeetingsService userMeetingsService)
        {
            _userMeetingsService = userMeetingsService;
        }

        [Authorize]
        [HttpGet("api/feature/dashboard/usermeetings", Name = "User Meeting List")]
        public IActionResult UserMeetingsResult()
        {
            var result = _userMeetingsService.Meetings(AuthUser.InfoResponse);
            if (result.Condition)
            {
                return Ok(result.Meetings);
            }
            return StatusCode(result.Code, result.Message);
        }
    }
}