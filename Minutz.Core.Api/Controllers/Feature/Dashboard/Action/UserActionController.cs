﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Minutz.Core.Api.Feature.Action;


namespace Minutz.Core.Api.Controllers.Feature.Dashboard.Action
{
    public class UserActionController : BaseController
    {
        private readonly IUserActionsService _userActionsService;

        public UserActionController(IUserActionsService userActionsService)
        {
            _userActionsService = userActionsService;
        }

        [Authorize]
        [HttpGet("api/feature/dashboard/useractions", Name = "User Actions List")]
        public IActionResult UserMeetingsResult()
        {
            var result = _userActionsService.Actions(AuthUser.InfoResponse);
            if (result.Condition)
            {
                return Ok(result.Actions);
            }
            
            return StatusCode(result.Code, result.Message);
        }
    }
}