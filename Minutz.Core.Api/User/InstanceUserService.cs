using System;
using System.Collections.Generic;
using System.Linq;
using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Extensions;
using Minutz.Core.Api.Models.Message;
using Minutz.Core.Api.Notification;
using Minutz.Core.Feature.Person;

namespace Minutz.Core.Api.User
{
    public class InstanceUserService: IInstanceUserService
    {
        private readonly IConnectionStringService _applicationSetting;
        private readonly IInstanceUserRepository _instanceUserRepository;
        private readonly INotificationService _notificationService;

        public InstanceUserService(
            IConnectionStringService applicationSetting,
            IInstanceUserRepository instanceUserRepository,
            INotificationService notificationService)
        {
            _applicationSetting = applicationSetting;
            _instanceUserRepository = instanceUserRepository;
            _notificationService = notificationService;
        }

        public PersonResponse GetInstancePeople(AuthRestModel user)
        {
            var result = new PersonResponse {Code = 500, Condition = false, Message = string.Empty, People = new List<Person>()};
            var masterConnectionString = _applicationSetting.CreateConnectionString();
            var peopleResult = _instanceUserRepository.GetInstancePeople(user.InstanceId, masterConnectionString);
            result.Condition = peopleResult.Condition;
            result.Code = peopleResult.Code;
            result.Message = peopleResult.Message;
            if (peopleResult.Condition)
                result.People = peopleResult.People;
            return result;
        }

        public PersonResponse AddInstancePerson(Person person, AuthRestModel user)
        {
            var result = new PersonResponse {Code = 500, Condition = false, Message = string.Empty, People = new List<Person>()};
            var instanceConnectionString = _applicationSetting.CreateConnectionString(user.InstanceId, _applicationSetting.GetInstancePassword(user.InstanceId));
            var masterConnectionString = _applicationSetting.CreateConnectionString();

            var instancePeople = _instanceUserRepository.GetInstancePeople(user.InstanceId, masterConnectionString);
            var personExists = instancePeople.People.FirstOrDefault(i => i.Email == person.Email);
            
            if (personExists == null)
            {
                var relatedCollection = new List<(string instanceId, string meetingId)> {(user.InstanceId, string.Empty)};
                person.Related = relatedCollection.ToRelatedString();
                var newPerson =
                    _instanceUserRepository.AddInstancePerson(person, user.InstanceId, masterConnectionString, instanceConnectionString);
                if (!newPerson.Condition)
                {
                    Console.WriteLine($"There was a issue adding the person, {newPerson.Message}");
                }
            }
            else
            {
                if (string.IsNullOrEmpty(personExists.Related)) personExists.Related = string.Empty;
                var related = personExists.Related.SplitToList(StringDividers.InstanceStringDivider, StringDividers.MeetingStringDivider);

                if (related.Count(i => i.instanceId == user.InstanceId) < 1)
                {
                    related.Add((user.InstanceId,string.Empty));
                    person.Related = related.ToRelatedString();
                    var updatePerson =
                        _instanceUserRepository.UpdateInstancePerson(person, user.InstanceId, masterConnectionString);
                    if (!updatePerson.Condition)
                    {
                        Console.WriteLine($"Could not update person record {updatePerson.Message}");
                    }
                }
            }
            var instancePerson = _instanceUserRepository
                .GetInstancePeople(user.InstanceId, masterConnectionString)
                .People
                .FirstOrDefault(i=> i.Email == person.Email);
            if (instancePerson != null)
            {
                try
                {
                    var attendee = new MeetingAttendee{
                        Name = instancePerson.FullName,
                        Email = instancePerson.Email,
                    };
                   var notificationResult = _notificationService.SendInstanceInvitation(attendee, user.InstanceId, user.Company);
                    if (!notificationResult.Condition)
                    {
                        Console.WriteLine($"There was a issue sending notification, {notificationResult.Message}");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("NOTE: Trying to send instance invitation.");
                    Console.WriteLine(e);
                }
            }

            if (instancePerson != null)
            {
                result.Code = 200;
                result.Person = instancePerson;
                result.Message = "Success";
            }
            else
            {
                result.Code = 404;
                result.Message = "The user cannot be found.";
            }

            return result;
        }
    }
}