using System.Collections.Generic;
using System.Linq;

namespace Minutz.Core.Feature.Related
{
    public class PersonInstanceValidation: IPersonInstanceValidation
    {
        public bool InstanceExists(List<(string instanceId, string meetingId)> collection, string instanceId)
        {
            var instanceObject = collection.Where(i => i.instanceId == instanceId);
            return instanceObject.Any();
        }
    }
}