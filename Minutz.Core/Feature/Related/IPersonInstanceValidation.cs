using System.Collections.Generic;

namespace Minutz.Core.Feature.Related
{
    public interface IPersonInstanceValidation
    {
        bool InstanceExists(List<(string instanceId, string meetingId)> collection, string instanceId);
    }
}