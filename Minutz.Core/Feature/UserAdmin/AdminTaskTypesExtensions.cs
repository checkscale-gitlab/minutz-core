using System;

namespace Minutz.Core.Feature.UserAdmin
{
    public static class AdminTaskTypesExtensions
    {
         public static AdminTasks ToAdminTask(this string value)
        {
            return (AdminTasks)Enum.Parse(typeof(AdminTasks), value, true);
        }
    }
}