using System.Data;
using System.Threading.Tasks;

namespace Minutz.Core.Feature.Person.Validation
{
    public interface IPersonValidationRepository
    {
        Task<MessageBase> ValidatePersonExistsAsync(IDbConnection connection, string schema, string email);
    }
}